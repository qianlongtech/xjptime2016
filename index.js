(function (swiperV, swiperH) {
    'use strict';
    var init, bind, dom, each;
    window.NodeList.prototype.each = function (callback) {
        var i = 0;
        for (i = 0; i < this.length; i += 1) {
            callback(i, this.item(i));
        }
    };
    window.Node.prototype.find = function (string) {
        return this.querySelectorAll(string);
    };
    dom = function (string) {
        return document.querySelectorAll(string);
    };
    bind = function () {
        var inner = dom('.text')[0],
            dialog = dom('.dialog')[0],
            arrow = dom('.arrow')[0];
        swiperH.on('transitionEnd', function (swiper) {
            (function (i) {
                inner.find('*').each(function (index, element) {
                    element.classList.remove('active');
                });
                inner.find('.city').item(i).classList.add('active');
                inner.find('.date').item(i).classList.add('active');
                inner.find('.desc').item(i).classList.add('active');
            }(swiper.activeIndex));
        });
        swiperV.on('tap', function () {
            dom('audio')[0].play();
        });
        swiperV.on('transitionEnd', function (swiper) {
            (function (i) {
                arrow.style.opacity = (i === 3) ? 0 : 1;
            }(swiper.activeIndex));
        });
    };
    init = function () {
        bind();
        dom('body')[0].style.opacity = 1;
        dom('audio')[0].play();
    };
    window.onload = init;
}(window.swiperV, window.swiperH));
